package adprog.teemeet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeemeetApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeemeetApplication.class, args);
	}

}
