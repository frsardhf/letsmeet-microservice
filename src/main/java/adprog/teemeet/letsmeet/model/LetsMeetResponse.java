package adprog.teemeet.letsmeet.model;

import lombok.NoArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

@Entity
@Table(name = "lets_meet_response")
@Data
@NoArgsConstructor
public class LetsMeetResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "response_id")
    private Long id;

    @Column(name = "username")
    private String name;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private LetsMeetEvent event;

    @ElementCollection
    @Column(name = "selected_time")
    private List<String> selectedTime;

    @Column(name = "responses")
    @ElementCollection
    private Map<String, ArrayList<String>> responses;
}