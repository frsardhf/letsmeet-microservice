package adprog.teemeet.letsmeet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.hibernate.annotations.ManyToAny;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(name = "lets_meet_event")
@Data
@NoArgsConstructor
@SequenceGenerator(name="seq", initialValue=135135, allocationSize=3)
public class LetsMeetEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq")
    @Column(name = "event_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date")
    private LocalDate date;

    @DateTimeFormat(pattern = "HH-mm")
    @Column(name = "start_time")
    private LocalTime startTime;

    @DateTimeFormat(pattern = "HH-mm")
    @Column(name = "end_time")
    private LocalTime endTime;

    @JsonIgnore
    @OneToMany(mappedBy = "event")
    private Set<LetsMeetResponse> response;
}