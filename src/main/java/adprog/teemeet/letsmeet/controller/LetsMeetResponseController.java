package adprog.teemeet.letsmeet.controller;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.service.MeetEventService;
import adprog.teemeet.letsmeet.service.MeetResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/response")
public class LetsMeetResponseController {

    @Autowired
    private MeetResponseService meetResponseService;

    @PostMapping(path = "/letsmeetevent/{id}/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postResponse(@PathVariable(value = "id") Long id,
                                  @PathVariable(value = "name") String name,
                                  @RequestBody LetsMeetResponse response) {
        return ResponseEntity.ok(meetResponseService.createMeetResponse(id, name, response));
    }

    @GetMapping(path = "/id/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getResponseById(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(meetResponseService.getMeetResponseById(id));
    }

    @GetMapping(path = "/letsmeetevent/{id}", produces = {"application/json"})
    public ResponseEntity getResponseEvent(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(meetResponseService.getListResponseByEvent(id));
    }
}
