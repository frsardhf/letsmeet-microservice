package adprog.teemeet.letsmeet.controller;


import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.service.MeetEventService;
import adprog.teemeet.letsmeet.service.MeetResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/letsmeetevent")
public class LetsMeetController {

    @Autowired
    private MeetEventService meetEventService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postEvent(@RequestBody LetsMeetEvent event) {
        return ResponseEntity.ok(meetEventService.createMeetEvent(event));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getEvent(@PathVariable(value = "id") Long id) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        if (event == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(event);
    }
}
