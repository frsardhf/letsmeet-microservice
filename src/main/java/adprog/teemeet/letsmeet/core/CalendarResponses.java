package adprog.teemeet.letsmeet.core;

import java.time.LocalTime;
import java.util.ArrayList;

public class CalendarResponses implements Calendar{
    private Long id;
    private String user;
    private ArrayList<String> selectedTime;
    private LocalTime startTime;
    private LocalTime endTime;

    public CalendarResponses(Long id, String user, ArrayList<String> selectedTime) {
        this.id = id;
        this.user = user;
        this.selectedTime = selectedTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ArrayList<String> getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(ArrayList<String> selectedTime) {
        this.selectedTime = selectedTime;
    }

    @Override
    public LocalTime getStartTime() {
        return startTime;
    }

    @Override
    public LocalTime getEndTime() {
        return endTime;
    }
}
