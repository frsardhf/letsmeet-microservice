package adprog.teemeet.letsmeet.core;

import java.time.*;

/**
 * Interface representing Calendar
 */
public interface Calendar {
    LocalTime getStartTime();
    
    LocalTime getEndTime();
}
