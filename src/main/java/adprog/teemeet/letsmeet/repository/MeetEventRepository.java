package adprog.teemeet.letsmeet.repository;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetEventRepository extends JpaRepository<LetsMeetEvent, Long>{
    LetsMeetEvent findByName(String name);
}
