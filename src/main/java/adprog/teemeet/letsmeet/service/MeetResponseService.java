package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface MeetResponseService {
    public LetsMeetResponse createMeetResponse(Long id,
                                               String name,
                                               LetsMeetResponse response);

    LetsMeetResponse getMeetResponseById(Long id);

    public Iterable<LetsMeetResponse> getListResponseByEvent(Long id);
}
