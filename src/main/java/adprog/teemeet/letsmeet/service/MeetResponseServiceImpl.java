package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.repository.MeetEventRepository;
import adprog.teemeet.letsmeet.repository.MeetResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MeetResponseServiceImpl implements MeetResponseService {
    @Autowired
    MeetResponseRepository meetResponseRepository;

    @Autowired
    MeetEventService meetEventService;

    @Override
    public LetsMeetResponse createMeetResponse(Long id,
                                               String name,
                                               LetsMeetResponse response) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        response.setEvent(event);
        response.setName(name);
        meetResponseRepository.save(response);
        return response;
    }

    @Override
    public LetsMeetResponse getMeetResponseById(Long id) {
        return meetResponseRepository.findById(id).get();
    }

    @Override
    public Iterable<LetsMeetResponse> getListResponseByEvent(Long id) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        List<LetsMeetResponse> responses = meetResponseRepository.findByEvent(event);
        return responses;
    }
}
