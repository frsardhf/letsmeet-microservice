package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface MeetEventService {
    LetsMeetEvent createMeetEvent(LetsMeetEvent event);

    LetsMeetEvent getMeetEventById(Long id);

    List<List<String>> getPartitionedMeetEventTimeSlots(LetsMeetEvent event);
}
