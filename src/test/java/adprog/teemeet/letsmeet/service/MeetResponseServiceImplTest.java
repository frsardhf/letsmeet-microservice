package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.repository.MeetResponseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class MeetResponseServiceImplTest {
    @Mock
    private MeetResponseRepository meetResponseRepository;

    @Mock
    private MeetEventService meetEventService;

    @Mock
    private MeetResponseService meetResponseService;

    private LetsMeetEvent letsMeetEvent;
    private LetsMeetResponse letsMeetResponse;
    private LetsMeetResponse letsMeetResponse2;

    @BeforeEach
    public void setUp() {
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13-00", timeFormatter));

        letsMeetResponse = new LetsMeetResponse();
        letsMeetResponse.setId(Long.parseLong("136136"));
        letsMeetResponse.setEvent(letsMeetEvent);
        List<String> selectedTime = Arrays.asList("15-00", "15-30");
        letsMeetResponse.setSelectedTime(selectedTime);
    }

    @Test
    public void testServiceCreateResponse(){
        lenient().when(meetResponseService.createMeetResponse(Long.parseLong("1"),
                "James", letsMeetResponse)).thenReturn(letsMeetResponse);
    }

    @Test
    public void testServiceGetResponseById(){
        lenient().when(meetResponseService.getMeetResponseById(Long.parseLong("1")))
                .thenReturn(letsMeetResponse);
        LetsMeetResponse response = meetResponseService.getMeetResponseById(Long.parseLong("1"));
        assertEquals(letsMeetResponse.getName(), response.getName());
    }

    @Test
    public void testServiceGetListResponseById(){
        List<LetsMeetResponse> allResponse = meetResponseRepository.findByEvent(letsMeetEvent);
        lenient().when(meetResponseService.getListResponseByEvent(letsMeetEvent.getId()))
                .thenReturn(allResponse);
        Iterable<LetsMeetResponse> allResponseResult = meetResponseService.getListResponseByEvent(Long.parseLong("135135"));
        Assertions.assertIterableEquals(allResponse, allResponseResult);
    }

}
