package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.repository.MeetEventRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MeetEventServiceImplTest {
    @Mock
    private MeetEventService meetEventService;

    private LetsMeetEvent letsMeetEvent;

    @BeforeEach
    public void setUp(){
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13-00", timeFormatter));
    }

    @Test
    public void testServiceCreateEvent(){
        lenient().when(meetEventService.createMeetEvent(letsMeetEvent)).thenReturn(letsMeetEvent);
    }

    @Test
    public void testServiceGetEventById(){
        lenient().when(meetEventService.getMeetEventById(Long.parseLong("135135")))
                .thenReturn(letsMeetEvent);
        LetsMeetEvent event = meetEventService.getMeetEventById(letsMeetEvent.getId());
        assertEquals(letsMeetEvent.getId(), event.getId());
    }
}
