package adprog.teemeet.letsmeet.controller;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.repository.MeetResponseRepository;
import adprog.teemeet.letsmeet.service.MeetEventService;
import adprog.teemeet.letsmeet.service.MeetResponseService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LetsMeetResponseController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
public class LetsMeetResponseControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MeetResponseService meetResponseService;

    @MockBean
    private MeetResponseRepository meetResponseRepository;

    @MockBean
    private MeetEventService meetEventService;

    private LetsMeetEvent letsMeetEvent;
    private LetsMeetResponse letsMeetResponse;

    @BeforeEach
    public void setUp() {
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13-00", timeFormatter));

        letsMeetResponse = new LetsMeetResponse();
        letsMeetResponse.setId(Long.parseLong("136136"));
        letsMeetResponse.setEvent(letsMeetEvent);
        List<String> selectedTime = Arrays.asList("15-00", "15-30");
        letsMeetResponse.setSelectedTime(selectedTime);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetResponseById() throws Exception {
        when(meetResponseService.getMeetResponseById(Long.parseLong("136136"))).
                thenReturn(letsMeetResponse);
        mvc.perform(get("/response/id/136136").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("136136"));
    }
}
